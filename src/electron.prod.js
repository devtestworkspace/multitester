
const builder = require("electron-builder")
const Platform = builder.Platform

// Promise is returned
builder.build({
  targets: Platform.WINDOWS.createTarget(),
  config: {
   "//": "build options, see https://goo.gl/QQXmcV",
   appId: "com.nttdata.multitester.multitester",
  }
})
  .then(() => {
    // handle result
  })
  .catch((error) => {
    // handle error
  })
