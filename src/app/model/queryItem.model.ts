
export type Method = 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH';

export class IQueryItem {
    name: string;
    url: string;
    method?: Method;
    params?: KeyValuePair[];
    body?: string;
    response?: any;
    ok?: boolean;
    validationExpression?: RegExp;
    extractExpressionsFromParent?: KeyRegExpPair[];

    subqueries?: QueryItem[];
    xMultitesterTraceId?: string;
}

export class QueryItem extends IQueryItem {
    
    constructor(queryItemParams?: IQueryItem) {
        super();
        Object.assign(this, queryItemParams);
    }

    parsedUrl():string {
        return this.parse(this.url);
    }

    parsedBody():string {
        return this.parse(this.body);
    }

    parsedName():string {
        return this.parse(this.name);
    }

    getMethod():Method {
        let method:Method = this.method;
        if (!method) {
            method = this.body ? 'POST' : 'GET';
        }
        return method;
    }

    assign(response, xMultitesterTraceId) {
        this.ok = true;
        this.response = response;
        this.xMultitesterTraceId = xMultitesterTraceId;
        if (this.validationExpression) {
            this.ok = this.validationExpression.test(response);
        }
        return this.ok;
    }

    fail(response) {
        this.ok = false;
        this.response = response;
    }

    override toString(): string {
        return "\nname:\n" + this.name 
            + "\n\nverb:\n" + this.method
            + "\n\nurl:\n" + this.parsedUrl()
            + "\n\nbody:\n" + this.parsedBody() 
            + "\n\nx-multitester-trace-id:\n" + this.xMultitesterTraceId
            + "\n\nresponse:\n" + this.response;
    }

    private parse(s) {
        let str = s || '';
        if (this.params) {
            for (let p of this.params) {
                str = str.replace('${' + p.key + '}', p.value);
            };
        }
        return str;
    }
}

export interface KeyValuePair {
    key: string;
    value: string;
}

export interface KeyRegExpPair {
    key: string;
    regExp: RegExp;
}