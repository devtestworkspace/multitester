import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, ViewChild, OnInit, NgZone, Inject } from '@angular/core';
import { QueryItem } from '@/app/model/queryItem.model';
import * as JSONEditor from 'jsoneditor';

@Component({
    selector: 'dialog-show-response',
    templateUrl: './dialog-new-query.html'
  })
  export class DialogNewQueryComponent implements OnInit {
    constructor(public dialogRef: MatDialogRef<DialogNewQueryComponent>, @Inject(MAT_DIALOG_DATA) public data: { }) { }

    public queryItem: QueryItem;
    private editor: JSONEditor;

    ngOnInit() {
      // create the editor
      const container = document.getElementById("jsoneditor");
      const options = {
        mode: 'code',
        modes: ['code', 'form', 'text', 'tree', 'view', 'preview'],
      };
      
      this.editor = new JSONEditor(container, options);
      
      let initialQueryItem =  new QueryItem({ name: 'customerView/residential', method: "GET", url: '${baseUrlApi}/daf2/customerView/v1/multitester/customerView/residential/${nifCif}?onlyActive=false',

      subqueries: [
        new QueryItem({ name: 'productInventory/${fixedLine}', method: "GET", url: '${baseUrlApi}/daf2/productInventory/v1/multitester/services/${fixedLine}?document=&lineCategory=fixed&onlyActive=false',
        extractExpressionsFromParent: [{ key: 'fixedLine', regExp: /fijo\s*Asociado",\s*"value":"([^"]+)"/g }] }),
      ] });
      // set json
      this.editor.set(initialQueryItem);

    }

    saveAndClose() {
      // get json
      this.queryItem = new QueryItem(this.editor.get());
      this.dialogRef.close(this.queryItem);
    }

  }