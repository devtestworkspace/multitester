import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, ViewChild, OnInit, NgZone, Inject } from '@angular/core';
import { QueryItem } from '@/app/model/queryItem.model';

@Component({
    selector: 'dialog-show-response',
    template: `<h2 mat-dialog-title>Query detail</h2>
    <mat-dialog-content class="mat-typography"><pre>{{data.query.toString()}}</pre></mat-dialog-content>
    <mat-dialog-actions align="end">
    <button mat-button (click)="data.copyToClipboard(data.query)">Copy to clipboard</button>
    <button mat-button [mat-dialog-close]="true" cdkFocusInitial>Close</button>
    </mat-dialog-actions>`
  })
  export class DialogShowResponseComponent {
    constructor(@Inject(MAT_DIALOG_DATA) public data: { query: QueryItem, copyToClipboard: any }) { }
  }