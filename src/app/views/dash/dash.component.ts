

import { Component, ViewChild, OnInit, NgZone, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { QueryItem } from '@/app/model/queryItem.model';
import { QueryService } from '@/app/services/query.service';
import { DialogShowResponseComponent } from '@/app/views/dialogs/dialog-show-response.component';
import { DialogNewQueryComponent } from '@/app/views/dialogs/dialog-new-query.component';
import { ClipboardService } from 'ngx-clipboard';

import { MatTable } from '@angular/material/table';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.scss'],
  providers: [ QueryService ]
})
export class DashComponent implements OnInit {


  @ViewChild('queriesTable')
  queriesTable: MatTable<any>;

  searchForm = this.fb.group({
    nifPas: ["41403498W"],
    environment: [],
  });

  exportFileInput: string = "";

  formData = {
    tiposDocumento: [{ id: 1, name: "NIF / CIF"}, { id: 2, name: "Pasaporte" }],
    environments: [
      {
        value: "UAT",
        env: {
          baseUrlApi: 'https://api-uat.int.si.multitester.es',
          baseUrlZuul: 'https://zuul-uat.int.si.multitester.es:9061'
        }
      },
      {
        value: "UAT2",
        env: {
          baseUrlApi: 'https://api-uat2.int.si.multitester.es',
          baseUrlZuul: 'https://zuul-uat2.int.si.multitester.es:9061'
        }
      },
      {
        value: "LOCALHOST",
        env: {
          baseUrlApi: 'http://localhost:4200',
          baseUrlZuul: 'http://localhost:4200'
        }
      },
      {
        value: "PRO",
        env: {
          baseUrlApi: 'https://api.si.multitester.es',
          baseUrlZuul: 'https://zuul.si.multitester.es:9061'
        }
      }
    ]
  };

  displayedColumns = ['name', 'method', 'url', 'body', 'response','ok']

  constructor(private fb: FormBuilder, private zone: NgZone, private queryService: QueryService, private copyClipboardService: ClipboardService, public dialog: MatDialog) {}

  queries:Array<QueryItem> = [
    
    new QueryItem({ name: 'customerManagement/customer', url: '${baseUrlApi}/daf2/customerManagement/v1/customer?brand=multitester&publicId=${nifCif}' }),
    // Controlar si es residencial o empresa 
    new QueryItem({ name: 'customerView/residential', url: '${baseUrlApi}/daf2/customerView/v1/multitester/customerView/residential/${nifCif}?onlyActive=false',

      subqueries: [
        new QueryItem({ name: 'productInventory/${fixedLine}', url: '${baseUrlApi}/daf2/productInventory/v1/multitester/services/${fixedLine}?document=&lineCategory=fixed&onlyActive=false',
        extractExpressionsFromParent: [{ key: 'fixedLine', regExp: /fijo\s*Asociado",\s*"value":"([^"]+)"/g }] }),
        new QueryItem({ name: 'productInventory/${fixedLine}', url: '${baseUrlApi}/daf2/productInventory/v1/multitester/services/${fixedLine}?document=&lineCategory=fixed&onlyActive=false',
        extractExpressionsFromParent: [{ key: 'msisdn', regExp: /MSISDN ASOCIADO","value":"([^"]+)"/g }] }),
      ] }),
    new QueryItem({ name: 'mdwTibcoService/115054', url: '${baseUrlZuul}/ms-tibcoconnection/v1/mdwTibcoService/115054', body: '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><XML_Peticion><CANAL>MICROSERVICIOS</CANAL><SistemaInvocante>FICHADECLIENTE</SistemaInvocante><PrimaryOrganization>RESIDENCIAL</PrimaryOrganization><ListOfSSAccount><AccountQuery><CSN>${nifCif}</CSN></AccountQuery></ListOfSSAccount></XML_Peticion>', validationExpression: /AccountData/g,
    subqueries: [
      new QueryItem({ name: 'mdwTibcoService/115020', url: '${baseUrlZuul}/ms-tibcoconnection/v1/mdwTibcoService/115020',
        body: '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><XML_Peticion><CANAL>MICROSERVICIOS</CANAL><SistemaInvocante>FICHADECLIENTE</SistemaInvocante><ListOfAsset><ListOfAssetQuery>            <recordcountneeded>Y</recordcountneeded>            <AssetMgmt-AssetQuery>                <AccountIntegrationId>${integrationId}</AccountIntegrationId>                <Organization>RESIDENCIAL</Organization>                <ListOfAssetMgmt-Asset>                    <recordcountneeded>Y</recordcountneeded>                    <AssetMgmt-Asset>                        <ListOfAssetMgmt-AssetXa>                            <recordcountneeded>Y</recordcountneeded>                            <AssetMgmt-AssetXa/>                        </ListOfAssetMgmt-AssetXa>                    </AssetMgmt-Asset>                </ListOfAssetMgmt-Asset>            </AssetMgmt-AssetQuery>        </ListOfAssetQuery>    </ListOfAsset></XML_Peticion>',
        validationExpression: /BillingAccountId/g,
        extractExpressionsFromParent: [{ key: 'integrationId', regExp: /IntegrationId>([^<]+)<\/IntegrationId/g }] })
    ] }),
   
  ];

  queriesView:Array<QueryItem> = [];

  search() {
    this.resetQueries();
    let nifCif:string = this.searchForm.controls['nifPas'].value;
    let environment:any = this.searchForm.controls['environment'].value;
    let baseUrlApi:string = '';
    let baseUrlZuul:string = '';
    nifCif = nifCif && nifCif.toUpperCase();
    if (environment && environment.env) {
      baseUrlApi = environment.env.baseUrlApi;
      baseUrlZuul = environment.env.baseUrlZuul;
    }
    this.queries.forEach(query => {
      query.params = [{ key: 'nifCif', value: nifCif },
      { key: 'baseUrlApi', value: baseUrlApi },
      { key: 'baseUrlZuul', value: baseUrlZuul }];
      this.queryService.executeQuery(query);
    });
    
  }

  

  importQueriesFromFile(event: any) {
    let files: FileList = event.target.files;
    let fileToUpload:File = files.item(0);
    console.log("fileToUpload", fileToUpload);
    const reader = new FileReader();
    reader.readAsText(fileToUpload);
    reader.onload = (event: any) => {
      console.log(event.target.result);
    }
    
  }

  exportQueriesToFile() {
    console.log(JSON.stringify(this.queriesView));
    let twoCharNumber = (n:number) => {
      let s:string = '' || n && n.toString();
      if (s.length == 1) {
        return "0" + s;
      }
      return s;
    }
    const blob = 
          new Blob([JSON.stringify(this.queriesView)], 
                   {type: "text/plain;charset=utf-8"});
    let d = new Date();
    let month = twoCharNumber((d.getMonth() +1));
    let date = twoCharNumber(d.getDate());
    
    let filename = "queries_" + d.getFullYear() + "-" + month + "-" + date + ".json";
    saveAs(blob, filename);
  }


  

  

  ngOnInit() {
    this.addQueries(this.queries);
    if (this.queriesTable) {
      this.queriesTable.renderRows();
    }
  }

  addQueries(queries: Array<QueryItem>) {
    queries.forEach(query => {
      this.queriesView.push(query);
      if (query.subqueries) {
        this.addQueries(query.subqueries);
      }
    });
  }

  resetQueries(subqueries = this.queries) {
    if (subqueries) {
      subqueries.forEach(subquery => {
        this.resetQuery(subquery);
      });
    }
  }
  resetQuery(query: QueryItem) {
    query.params = [];
    query.response = '';
    query.ok = null;
    this.resetQueries(query.subqueries || []);
  }

  copyToClipboard(query: QueryItem) {
    this.copyClipboardService.copyFromContent(query.toString());
  }

  openDialog(query: QueryItem) {
    const dialogRef = this.dialog.open(DialogShowResponseComponent, { data: { query, copyToClipboard: (args) => this.copyToClipboard(args) } });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  newQueryDialog() {
    const dialogRef = this.dialog.open(DialogNewQueryComponent, { data: { } });

    dialogRef.afterClosed().subscribe(result => {
      this.queriesView.push(result);
      if (this.queriesTable) {
        this.queriesTable.renderRows();
      }
    });
  }

}



