import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { QueryItem } from '@/app/model/queryItem.model';
import { environment } from '@/environments/environment';
import { catchError, retry } from 'rxjs/operators';
import { throwError } from 'rxjs';

/**
 * Service for user file
 * @export QueryService
 * @class QueryService
 * @implements {Injectable}
 */
@Injectable()
export class QueryService {


  headers: any =  {
    "OAM_SFID": "53000053",
    "X-FORWARDED-SERVER": "multitester.es",
    "OAM_TIENE_PERFIL": "cn=PDV,ou=PANGEA,ou=Web,ou=Perfiles,o=amena.es",
    "OAM_REMOTE_USER": "PAN_PDV",
    'rejectUnauthorized': 'false',
    'requestCert': 'false'
  };

  
  constructor(private http: HttpClient) {}

  public executeQuery(query: QueryItem) {

    query.response = "Loading...";
    return this.http.request(query.getMethod(), query.parsedUrl(), {
      headers: this.headers,
      body: query.body,
      observe: 'response',
      responseType: 'text'
    }).pipe(
      catchError(this.handleError), // then handle the error
    ).subscribe({ next: this.processQueryResult(query), error: this.processQueryError(query)});
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        'Backend returned error, ', error);
    }
    // Return an observable with a user-facing error message.
    return throwError(() => error);
  }

  private processQueryError(query: QueryItem): (error: any) => void {
    return (error) => {
      query.fail(JSON.stringify(error));
    };
  }

  private processQueryResult(query: QueryItem): (data: HttpResponse<string>) => void {
    return (data: HttpResponse<string>) => {
      console.log("data", data);
      query.assign(data.body, data.headers.get('x-multitester-trace-id'));
      this.processSubqueries(query);

      let headers = data.headers;
      if (query.assign(data.body, headers && headers.get("x-multitester-trace-id"))) {
        this.processSubqueries(query);
      }
      
    };
  }

  private processSubqueries(query: QueryItem) {
    if (query.subqueries) {
      query.subqueries.forEach(subquery => {
        if (subquery.extractExpressionsFromParent) {
          subquery.params = query.params;
          try {
            subquery.extractExpressionsFromParent.forEach(expression => {
              let coincidences = query.response.matchAll(expression.regExp);
              for (let coincidence of coincidences) {
                console.log("coincidence", coincidence);
                subquery.params.push({ key: expression.key, value: coincidence[1] });
              }
            });
          } catch (error) {
            console.log("error", error);
          }
          
        }
        this.executeQuery(subquery);
      });
    }
  }

}

